require_relative 'my_object_store'

class Play
  include MyObjectStore

  attr_accessor :age, :lname, :fname, :email

  def initialize(age = 1, fname = 'fname', lname = 'lname', email = 'email@email.com')
    @age = age
    @fname = fname
    @lname = lname
    @email = email
  end

  def validate?
    age > 1
  end
end
