module MyObjectStore

  module ClassMethods
    include Enumerable

    def each(&block)
      @my_objects_array.each &block
    end

    def add_object_to_array(object)
      @my_objects_array << object
    end

    def method_missing(name, *args)
      attribute = name.to_s.partition('find_by_').last
      attribute_accessor = attribute.to_sym
      if method_defined?(attribute_accessor)
        match_result = []
        for obj in @my_objects_array
          if args.first == obj.public_send(attribute_accessor)
            match_result << obj
          end
        end
        return match_result.to_enum
      end
      super
    end

  #class_method module ends here
  end

  def self.included(klass)
    klass.extend ClassMethods
    klass.instance_variable_set(:@my_objects_array, [])
  end

  def save
    unless respond_to?(:validate?) && !validate?
      self.class.add_object_to_array(self)
    end
  end

end
