module Chain

  def chained_aliasing(old_name, new_name)
    post_name = new_name.to_s
    pre_name = old_name.to_s
    
    if ['?', '!', '='].include?(pre_name[-1])
      post_name = post_name + pre_name[-1]
      pre_name = pre_name.chop
    end

    alias_method pre_name + '_without_' + post_name, old_name
    alias_method old_name, pre_name + '_with_' + post_name
  end

end