require_relative './chain'

class Hello
  extend Chain

  def greet_with_logger
    puts '--logging start'
    greet_without_logger
    puts '--logging end'
  end

  def greet
    puts 'hello'
  end

  chained_aliasing :greet, :logger
end