animal = "cat"

def animal.speak
  puts "miaow"
end

animal.speak

animal = "dog"

#comment the below written statement before checking the second part of code
animal.speak #throws undefined method error

#second part
class << animal
  def speak
    puts 'bhaow'
  end
end

animal.speak

animal = 'monkey'
animal.speak #throws undefined method error
