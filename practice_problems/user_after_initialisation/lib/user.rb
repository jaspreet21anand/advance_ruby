require_relative './initialize'

class User
  include Init
  attr_accessor :name

  hello after_initialize: :abc
  def initialize(name)
    @name = name
  end

  def abc
    puts 'abc'
  end
end


p User.new('adi sir').name
