module Init

  def self.included(klass)
    klass.extend ClassMethods
    klass.send(:prepend, InstanceMethods)
  end

  module ClassMethods
    def hello(options)
      @options = options
    end
  end

  module InstanceMethods
    def initialize(*args)
      method(self.class.instance_variable_get(:@options)[:after_initialize]).call
      super
    end
  end

end