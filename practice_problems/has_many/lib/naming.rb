module Naming
  def self.singularize(name)
    name.chomp 's'
  end
end
