require_relative './assoc'

class Library
  extend Assoc

  has_many :books, class_name: 'Book'
  has_many :chairs

  def initialize(city)
    @city = city
  end
end
