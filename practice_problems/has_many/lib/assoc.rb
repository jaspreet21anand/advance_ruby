require_relative './naming'

module Assoc

  def self.extended(klass)
    klass.send(:prepend, InstanceModule)
  end

  module InstanceModule
    
    def initialize(*args)
      methods_array = self.class.instance_methods.select { |name| name.to_s =~ /=$/ }
      methods_array.each { |meth| send(meth, []) }
      super
    end

  end

  def has_many(object, options = {})
    class_name = options[:class_name] || Naming.singularize(object.to_s)
    attr_accessor object
    
    define_method("create_#{ class_name.downcase }") do |value|
      send("#{ object.to_s }").send(:<<, Object.const_get(class_name.capitalize).new(value))
    end

  end

end