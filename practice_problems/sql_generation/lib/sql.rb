module SQL

  def where(conditional_hash)
    @conditions ||= {}
    @conditions = @conditions.merge(conditional_hash)
    self
  end

  def select(*columns)
    @selector ||= []
    columns.each do |value| 
      @selector << value.to_s
    end
    self
  end

  def to_sql
    conditions_str = ' '
    if @conditions
      @conditions.each_pair do |key, value|
        if value.kind_of? Fixnum
          str = "#{ value }"
        else
          str = "'#{ value }'"
        end
        conditions_str = conditions_str + "#{ key.to_s }=" + str + ' AND '
      end
    end
    conditions_str.strip!.chomp! 'AND'
    if(@selector)
      selector_str = @selector.join(',')
    else
      selector_str = '*'
    end
    "SELECT #{ selector_str } FROM #{ self.to_s.downcase + 's' } WHERE #{ conditions_str }".strip.chomp 'WHERE'
    ensure
      @selector = nil
      @conditions = nil
  end

end
