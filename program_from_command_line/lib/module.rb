module Create

  def create_method(name, code)
    define_method(name) do
      eval code
    end
  end

end
