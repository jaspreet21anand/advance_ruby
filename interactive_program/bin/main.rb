require_relative '../lib/interactive_code'

interactive_coder = InteractiveCode.new

while true

  begin
    interactive_coder.start_interactive_coding(prompt_symbol = '>>> ')
  rescue StandardError, ScriptError => e
    puts e.message
  end

end
