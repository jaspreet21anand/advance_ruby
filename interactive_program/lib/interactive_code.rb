class InteractiveCode
  
  def get_code_input_from_user(prompt_symbol)
    code = ''
    input = ''
    while input != "\n"
      print prompt_symbol
      code = code + input
      input = gets
      if input.strip == 'q'
        exit
      end
    end
    code
  end

  def start_interactive_coding(prompt_symbol)
    code = get_code_input_from_user(prompt_symbol)
    puts eval(code, TOPLEVEL_BINDING)
  end

end
