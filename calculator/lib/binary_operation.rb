class BinaryOperation

  def self.calculate(first_operand, operator, second_operand)
    first_operand.send(operator, second_operand)
  end

end
