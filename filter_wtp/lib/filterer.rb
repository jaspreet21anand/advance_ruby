require_relative './after_filter'
require_relative './before_filter'

module Filterer

  def self.included(klass)
    klass.singleton_class.send(:attr_reader, :filters)
    klass.instance_variable_set(:@filters, Hash.new { |hash, key| hash[key] = [] })
    klass.extend ClassMethods
  end

  module ClassMethods

    def before_filter(*args)
      create_filter(args, 'before')
    end

    def after_filter(*args)
      create_filter(args, 'after')
    end

    private
    def method_added(name)
      return if @creating_method
      @creating_method = true
      apply_filters_to(name)
      @creating_method = false
    end

    def create_filter(args, type)
      conditions = args.last.kind_of?(Hash) ? args.pop : {}
      args.each do |method_name|
        filters[type.to_sym] << Filter.build(type, method_name, conditions)
      end
    end

    def apply_filters_to(name)
      original_method = instance_method(name)
      define_method(name) do
        call_filters(:before, name) if filter_allowed?(name)
        original_method.bind(self).call
        call_filters(:after, name) if filter_allowed?(name)
      end
    end

  end

  private
  def call_filters(type, name)
    self.class.filters[type.to_sym].each do |filter|
      filter.call(self, name)
    end
  end

  def filter_allowed?(name)
    !self.class.filters.any? do |key, filters|
      filters.any? { |filter| filter.name == name }
    end
  end

end