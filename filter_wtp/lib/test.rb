require_relative './filterer'

class Test
  include Filterer
  after_filter :yy, except: [:foo]

  def yy
    p 'yy'
  end

  def a
    p 'a'
  end
  after_filter :foo
  before_filter :a

  def foo
    p 'foo'
  end
  
  def ab
    p 'ab'
  end

  def cd
    p 'cd'
  end
end


class Test
  before_filter :cd
  def reopen
    p 'reopen'
  end
end
