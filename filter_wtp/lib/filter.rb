class Filter
  attr_reader :name

  def self.build(type, name, *args)
    Object.const_get(type.capitalize + 'Filter').new(name, *args)
  end

  def initialize(name, conditions = {})
    @name = name
    @conditions = conditions
  end

  def call(object, method_name)
    if @conditions[:only]
      if @conditions[:only].include? method_name
        object.send(@name)
      end
    elsif @conditions[:except].nil? || !@conditions[:except].include?(method_name)
      object.send(@name)
    end
  end
end
