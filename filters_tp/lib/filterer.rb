require_relative './filter'
module Filterer
  
  def self.extended(klass)
    klass.instance_variable_set(:@filters, { universal:[], specific:[] })

    trace = TracePoint.new(:end) do |tp|
      klass.instance_variable_set(:@stored_instance_methods, klass.instance_methods(false))
      filters = klass.instance_variable_get(:@filters)
      (filters[:universal] + filters[:specific]).each do |filter|
        filter.apply(klass.instance_variable_get(:@stored_instance_methods))
      end

      class << klass
        define_method(:method_added) do |name|
          return if @creating_method
          @stored_instance_methods << name
          @filters[:universal].each do |filter|
            filter.apply([name])
          end
        end
      end
      
      tp.disable
    end
    trace.enable
  end

  def before_filter(*args)
    create_filter(args, 'before')
  end

  def after_filter(*args)
    create_filter(args, 'after')
  end

  def create_filter(args, type)
    conditional_hash = args.pop if args.last.kind_of? Hash
    conditional_hash = {} if !conditional_hash
    if conditional_hash[:only]
      @filters[:specific] << Filter.new(filters = args, type = type, klass = self, conditional_hash)
    else
      @filters[:universal] << Filter.new(filters = args, type = type, klass = self, conditional_hash)
    end
  end

end
