class Filter
  @filters = Hash.new { |hash, key| hash[key] = [] }

  def initialize(filters, type, klass, conditional_hash = {})
    @filters = filters
    @type = type
    @conditional_hash = conditional_hash
    @klass = klass
    @conditional_hash.default = []
    self.class.instance_variable_get(:@filters)[@klass] += filters
  end

  def apply(methods_to_be_filtered)
    @klass.instance_variable_set(:@creating_method, true)

    unless @conditional_hash[:only].empty?
      methods_to_be_filtered = @conditional_hash[:only]
    else
      methods_to_be_filtered = methods_to_be_filtered - @conditional_hash[:except]
    end
    
    methods_to_be_filtered.each do |meth|
      unless self.class.instance_variable_get(:@filters)[@klass].include? meth
        filters = @filters
        type = @type
        id = self.object_id
        @klass.send(:alias_method, "___#{ meth }#{ id }___", meth)
        
        @klass.class_eval do
          define_method(meth) do
            send("___#{ meth }#{ id }___") if type.eql? 'after'
            filters.each do |filter_method|
              send(filter_method)
            end
            send("___#{ meth }#{ id }___") if type.eql? 'before'
          end
        end
      
      end
    end

    @klass.instance_variable_set(:@creating_method, false)
  end

end
