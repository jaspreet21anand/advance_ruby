require_relative './filterer'

class Test
  extend Filterer

  def yy
    p 'yy'
  end

  def a
    p 'a'
  end
  after_filter :yy, except: [:foo]
  after_filter :foo
  before_filter :a, only: [:ab]

  def foo
    p 'foo'
  end
  
  def ab
    p 'ab'
  end

  def cd
    p 'cd'
  end
end
