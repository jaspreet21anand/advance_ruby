require_relative '../lib/shopping_list'

shopping_list = ShoppingList.new

shopping_list.items do
  add("toothpaste", 2)
  add("computer", 1)
end

puts shopping_list
puts shopping_list.item_list
