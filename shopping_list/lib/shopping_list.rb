require_relative '../lib/item'

class ShoppingList

  attr_reader :item_list

  def initialize
    @item_list = []
  end

  def items(&block)
    instance_eval(&block)
  end

  def add(name, qty)
    @item_list << Item.new(name, qty)
  end

  def to_s
    "#{ @item_list }"
  end

end
