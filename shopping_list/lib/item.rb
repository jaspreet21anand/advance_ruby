class Item

  def initialize(name, qty)
    @name = name
    @qty = qty
  end

  def to_s
    "#{ @name }, qty: #{ @qty }"
  end

end