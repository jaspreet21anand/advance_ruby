class AA
  def a
    p 'a'
  end
  def b
    p 'b'
  end
  alias_method :o, :a
  alias_method :o, :b
end

p AA.new.method(:o)