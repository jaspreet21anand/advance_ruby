# module Filter

#   def self.extended(klass)
#     klass.instance_variable_set(:@before_filter, [])
#     klass.instance_variable_set(:@before_hash, Hash.new { |hash, key| hash[key] = [] })
#     klass.instance_variable_set(:@after_filter, [])
#     klass.instance_variable_set(:@after_hash, Hash.new { |hash, key| hash[key] = [] })
#     klass.instance_variable_set(:@native_methods, [])
#     klass.instance_variable_set(:@creating_method, false)
#     # t = TracePoint.new(:end) do |tp|
#     #   klass.create_method
#     # end
#     # t.enable
#   end

#   def method_added(name)
#     # p name
#     # p !(name.to_s=~/prev/)
#     # if !(/prev/.match(name.to_s))
#     # p @creating_method
#     return if @creating_method
#     #   before_filter
#     #   p 'method added'
#     # end
#     @creating_method = true
#     alias_method "__#{ name }__", name
#     @native_methods << "native_#{ name }".to_sym
#     @creating_method = false
#     p @native_methods
#   end

#   def create_method(meth, before)
#     # new_methods = instance_methods(false) - @before_filter - @after_filter - @before_hash[:except] - @after_hash[:except]
#     # p new_methods
#     # @true_false = true
#     # new_methods.each do |meth|
#       # p meth
#       # alias_method "__#{ meth }__", meth
#       define_method(meth) do |*args|
#         # p self.class.instance_variable_get(:@before_filter)
#         # p self.class.instance_methods(false)
#         # if before
#             self.class.instance_variable_get(:@before_filter).each do |method|
#             # p method
#             send(meth)
#           end
#         # end
#         send("__#{ meth }__")
#         # if !before
#           self.class.instance_variable_get(:@after_filter).each do |method|
#             send(method)
#           end
#         # end
#       end
#     # end
#   end

#   def before_filter(*args)
#     @before_hash = args.pop if args.last.kind_of? Hash
#     @before_filter = args
#     # create_method
#   end

#   def after_filter(*args)
#     @after_hash = args.pop if args.last.kind_of? Hash
#     @after_filter = args
#     # create_method
#   end
# #   def before_filter(*args)
# #     @before_methods ||= []
# #     @before_methods = args if !args.empty?
# #     if args.last.kind_of? Hash

# #     else
# #       @creating_method = true
# #       p instance_methods(false)
# #       (instance_methods(false) - @before_methods).each do |meth|
# #         # p @before_methods
# #         alias_method "prev_#{ meth }", meth
# #         define_method(meth) do
# #           args.each do |meth|
# #             send(meth)
# #           end
# #           send("prev_#{ meth }")
# #         end
# #       end
# #     end
# #     @creating_method = false
# #   end
# # end

#   # module PrependClass
#   # end

# #   def method_added(name)
# #     before_filter
# #     # after_filter
# #   end

# #   def before_filter(*args)
# #     if !args.empty? && !@before_filter
# #       @before_filter = args
# #       @before_hash = @before_filter.last if @before_filter.last.kind_of? Hash
# #       @before_filter.pop if @before_filter.last.kind_of? Hash
# #     end
# #     if @before_filter
# #       args = @before_filter
# #     end
# #     # p args
# #     klass = self
# #     if @before_hash.kind_of? Hash
# #       selector = @before_hash
# #       # args.pop
# #       # p 'selector', selector
# #       PrependClass.module_eval do
# #         if selector[:only]
# #           selector[:only].each do |meth|
# #             define_method(meth) do |*arguments|
# #               args.each do |meth|
# #                 send(meth)
# #               end
# #               super(*arguments)
# #             end
# #           end
# #         elsif selector[:except]
# #           # p selector[:except]
# #           # p 'entered except', klass.instance_methods(false) - selector[:except] - args
# #           (klass.instance_methods(false) - selector[:except] - args).each do |meth|
# #             define_method(meth) do |*arguments|
# #           # p 'entered except'
# #               args.each do |meth|
# #                 send(meth)
# #               end
# #               super(*arguments)
# #             end
# #           end
# #         end
# #       end
# #     elsif !@before_hash
# #       # p 'entered else before_hash'
# #       PrependClass.module_eval do
# #         # p klass.instance_methods(false) - args
# #         (klass.instance_methods(false) - args).each do |meth|
# #       # p args
# #           define_method(meth) do |*arguments|
# #             args.each do |meth|
# #               send(meth)
# #             end
# #             super(*arguments)
# #           end
# #         end
# #       end
# #     end
# #     send(:prepend, PrependClass)
# #   end

# #   def after_filter(*args)
# #     if !args.empty? && !@after_filter
# #       @after_filter ||= args
# #       @after_hash = @after_filter.last if @after_filter.last.kind_of? Hash
# #       @after_filter.pop if @after_filter.last.kind_of? Hash
# #     end
# #     if @after_filter
# #       args = @after_filter
# #       # p 'print args'
# #       # print 'args assignment'
# #       # p args
# #     end
# #     klass = self
# #     if @after_hash.kind_of? Hash
# #       selector = @after_hash
# #       PrependClass.module_eval do
# #         if selector[:only]
# #           selector[:only].each do |meth|
# #             define_method(meth) do |*arguments|
# #               super(*arguments)
# #               args.each do |meth|
# #                 send(meth)
# #               end
# #             end
# #           end
# #         elsif selector[:except]
# #           (klass.instance_methods(false) - selector[:except] - args).each do |meth|
# #             define_method(meth) do |*arguments|
# #               super(*arguments)
# #               args.each do |meth|
# #                 send(meth)
# #               end
# #             end
# #           end
# #         end
# #       end
# #     elsif !@after_hash
# #       PrependClass.module_eval do
# #         # p klass.instance_methods(false) - args
# #         (klass.instance_methods(false) - args).each do |meth|
# #           define_method(meth) do |*arguments|
# #             super(*arguments)
# #             args.each do |meth|
# #               send(meth)
# #             end
# #           end
# #         end
# #       end
# #     end
# #     send(:prepend, PrependClass)
# #   end
# end