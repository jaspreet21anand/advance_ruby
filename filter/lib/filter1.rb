module Filter

  def self.extended(klass)
    klass.instance_variable_set(:@before_filter, [])
    klass.instance_variable_set(:@before_hash, Hash.new { |hash, key| hash[key] = [] })
    klass.instance_variable_set(:@after_filter, [])
    klass.instance_variable_set(:@after_hash, Hash.new { |hash, key| hash[key] = [] })
    klass.instance_variable_set(:@native_methods, [])
    klass.instance_variable_set(:@creating_method, false)
  end

  def method_added(name)
    return if @creating_method
    @native_methods << name
    unless(@before_filter.empty?)
      before_filter(name:name)
    end
    unless(@after_filter.empty?)
      after_filter(name:name)
    end
  end

  def create_method(name, before)
    @creating_method = true
    if before
      alias_method "__#{ name }__", name
      define_method(name) do |*args|
        self.class.instance_variable_get(:@before_filter).each do |bm|
          send(bm)
        end
        send("__#{ name }__")
      end
    else
      alias_method "__#{ name }___", name
      define_method(name) do |*args|
        send("__#{ name }___")
        self.class.instance_variable_get(:@after_filter).each do |bm|
          send(bm)
        end
      end
    end
    @creating_method = false
  end

  def before_filter(*args)
    @before_hash = args.pop if args.last.kind_of? Hash
    @before_filter += args
    
    if !@before_hash[:name].empty? && !(@before_filter + @after_filter).include?(@before_hash[:name])
      create_method(@before_hash[:name], true)
      @before_hash[:name] = []
    else
      (@native_methods - @before_filter - @after_filter).each do |method|
        create_method(method, true)
      end
    end
  end

  def after_filter(*args)
    @after_hash = args.pop if args.last.kind_of? Hash
    @after_filter += args

    if !@after_hash[:name].empty? && !(@after_filter + @before_filter).include?(@after_hash[:name])
      create_method(@after_hash[:name], false)
      @after_hash[:name] = []
    else
      (@native_methods - @after_filter - @before_filter).each do |method|
        create_method(method, false)
      end
    end
  end

end