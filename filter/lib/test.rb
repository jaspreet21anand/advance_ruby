require_relative './filter1'

class Test
  extend Filter

  def yy
    p 'yy'
  end

  def a
    p 'a'
  end
  # after_filter :a

  def foo
    p 'foo'
  end
  
  def ab
    p 'ab'
  end
  after_filter :a
  before_filter :yy
  # p @before_filter
  # p @after_filter
  # create_method
end
# p Test.instance_method(:ab)
# p Test.instance_variable_get(:@before_hash)
h = Test.new
h.ab
p Test.instance_methods(false)