require 'csv'
require_relative './class_factory'

class CSVFile

  def initialize(file_path)
    @file_path = file_path
    @file_name = File.basename(file_path, ".csv")
  end

  def get_headers
    headers = nil
    CSV.foreach(@file_path) { |row| headers = row; break }
    headers
  end

  def create_objects
    klass = Object.const_get(@file_name.capitalize)
    object_array = []
    get_records.each { |record| object_array << klass.new(*record) }
    object_array
  end

  def create_class
    @class = ClassFactory.new(@file_name, get_headers)
  end

  def get_records
    records = CSV.read("#{ @file_path }")
    records.drop(1)
  end

end
