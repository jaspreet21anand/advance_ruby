module Nomenclature
  @replacement_hash = { '/[- ]/' => '_'}

  def self.modify_attribute_name(text)
    replaced_text = text
    @replacement_hash.each_pair do |key, value|
      replaced_text = replaced_text.gsub(key, value)
    end
    replaced_text
  end
end
