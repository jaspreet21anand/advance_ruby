require_relative './nomenclature'

class ClassFactory

  def self.new(name, attributes)
    attributes = attributes.map {|attribute| Nomenclature.modify_attribute_name(attribute) }
    klass = Object.const_set(name.capitalize, Class.new do
      send(:attr_accessor, *attributes)
    end)

    klass.class_eval do
      define_method(:initialize) do |*args|
        attributes.each_with_index do |attribute, i|
          send("#{ attribute }=", args[i])
        end
      end
    end
    klass
  end

end
