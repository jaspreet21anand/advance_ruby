require_relative '../lib/sub_string'

print 'Enter the string: '
user_input_string =SubString.new(gets.chomp)

print 'choose one method from following List of methods: '
SubString.instance_methods(false).each do |meth|
  print "#{ meth }("
  user_input_string.method(meth).parameters.each do |params|
    case params[0]
    when :req
      print "#{ params[1] } "
    when :opt
      print "[#{ params[1] }] "
    when :rest
      print "*#{ params[1] } " 
    end
  end
  puts ")"
end

print 'method: '
puts user_input_string.instance_eval gets.chomp
