class SubString < String

  def exclude?(sub_string)
    !include?(sub_string)
  end

  def reverse_order_of_words
    split.reverse.join(' ')
  end

end