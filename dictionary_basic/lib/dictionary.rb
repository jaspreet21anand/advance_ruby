require_relative './string'
class Dictionary

  @english = {
    a: ['answer', 'ansder', 'apiber'],
    b: [],
    c: [],
    r: ['remembarance', 'reminance', 'regular', 'ready']
  }
  singleton_class.send(:attr_reader, :english)

  def initialize(dictionary_name)
    @dictionary_name = dictionary_name.downcase
  end

  def search_word(str, no_of_suggestions)
    match_substring(str).first(no_of_suggestions)
  end

  private
  def match_substring(str)
    matches = []
    relevant_words = self.class.public_send(@dictionary_name)[str[0].to_sym]
    str.length.downto(1) do |len|
      str.substrings_of_size(len).each do |sub_str|
        relevant_words.each do |word|
          matches.push(word) if Regexp.new(sub_str) =~ word
        end
      end
      return matches unless matches.empty?
    end
    ['no match']
  end

end
