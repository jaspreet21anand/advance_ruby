class String

  def substrings_of_size(size)
    sub_strings = []
    (length - size + 1).times do |offset|
      sub_strings.push(slice(offset, size))
    end
    sub_strings
  end

end
